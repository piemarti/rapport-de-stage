\chapter{Grammaires catégorielles de dépendances} % ============================

\section{Grammaires catégorielles}

Les grammaires catégorielles\footcite{moot_logic_2012} sont généralement
considérées comme des grammaires de constituants. Cependant, la grammaire de
Hays-Gaifman est définie de façon très similaire. Il est donc assez naturel
d'utiliser les grammaires catégorielles pour définir une grammaire de
dépendances.

Pour analyser une phrase avec une grammaire catégorielle, on associe une {\bf
catégorie}, ou un {\bf type}, à chaque mot de la phrase. La grammaire définit un
ensemble de types primitifs dont fait partie le type de la phrase, noté $s$. On
définit également des types composés de types primitifs, en particulier le type
fonction $a \ldep b$ (resp. $b \rdep a$) qui prend un argument de type $a$ à
gauche (resp. à droite) et produit un résultat de type $b$.

On déduit le type d'un groupe de mots grâce à un ensemble de règles de
déduction, par exemple:
\startformula
  \frac{u : a \quad v : a \ldep b}
       {uv : b} \ldep_e
\stopformula
La grammaire catégorielle associe à chaque mot un ensemble de types possibles.
Analyser une phrase revient à choisir un type parmi cet ensemble pour chaque mot
de sorte à ce que le type de la phrase entière soit $s$.

\placefigure{Une déduction dans le Lambek-calcul} {
  \startformula

  \lelim{
    \relim{
      \rintro{
        \lelim{
          \relim{
            \lelim{
              \word{Nous}   {np}
              \word{sommes} {np \ldep s \rdep np}
            }{s \rdep np}

            \hyp{np}
          }{s}

          \word{tous}{s \ldep s}
        }{s}
      }{s \rdep np}

      \relim{
        \word{des}     {np \rdep nn}
        \word{fleurs}  {nn}
      }{np}
    }{s}

    \relim{
      \word{dans}    {s \ldep s \rdep np}

      \relim{
        \word{le}      {np \rdep n}

        \relim{
          \word{même}    {n \rdep n}
          \word{bouquet} {n}
        }{n}
      }{np}
    }{s \ldep s}
  }{s}

  \stopformula
}
\section{Les dépendances comme catégories}

Dans \cite[title][dikovsky_dependencies_2004],
\cite[authoryears][dikovsky_dependencies_2004] propose d'utiliser les noms des
dépendances comme catégories d'une grammaire catégorielle.

\placefigure[here,none]
{}
{
  \switchtobodyfont[10pt]

  \startnodes[dx=1em,dy=2.75ex,alternative=arrow]

    \cat { 0}   {Une}        {Une}        {$det$}
    \cat { 5}   {citrouille} {citrouille} {$\rppol{det \ldep suj}{acl}$}
    \cat { 9.5} {est}        {est}        {$aux$}
    \cat {15}   {apparue}    {apparue}    {$suj \ldep aux \ldep S \rdep \ranchor{acl}$}
    \cat {20}   {qui}        {qui}        {$suj$}
    \cat {23}   {n}          {n'}         {$adv$}
    \cat {27.5} {etait}      {était}      {$adv \ldep cop \rdep adv$}
    \cat {32}   {pas}        {pas}        {$adv$}
    \cat {39}   {la}         {là}         {$\rnpol{suj \ldep cop \ldep \ranchor{acl} \rdep adv}{acl}$}
    \cat {46}   {avant}      {avant.}     {$adv$}

    \placenode [15,4] [reference=root] {\node{\tt root}}

    \rootis{apparue}

    \depend{citrouille, Une}        {det} {-0.35}
    \depend{citrouille, la}         {acl} { 0.35}
    \depend{apparue,    citrouille} {suj} {-0.2}
    \depend{apparue,    est}        {aux} { 0.2}
    \depend{etait,      n}          {adv} { 0.35}
    \depend{etait,      pas}        {adv} {-0.35}
    \depend{la,         qui}        {suj} {-0.2}
    \depend{la,         etait}      {cop} {-0.2}
    \depend{la,         avant}      {adv} { 0.2}

    \connectnodes [apparue, la][label={$\anc{\prn{acl}}$}, offset=0.25, option=dashed]
  \stopnodes
}

Les catégories primitives de la grammaire catégorielle de dépendances
contiennent les noms des dépendances issues de l'analyse en grammaire de
dépendances. On note $C$ l'ensemble des noms de dépendances disponibles pour
construire une catégorie, dont fait partie $S$, la catégorie de la racine. En
présence de dépendances projectives, on construit la catégorie du mot $w$ de la
façon suivante:
\startitemize
\item soient $l_1,…,l_k \in C^{k}$ les noms des dépendances dans lesquelles $w$
      est gouverneur et qui pointent à gauche de $w$;
\item soient $r_1,…,r_m \in C^{m}$ les noms des dépendances dans lesquelles $w$
      est gouverneur et qui pointent à droite de $w$;
\item soit $h \in C$ le nom de la dépendance à laquelle $w$ est subordonné, ou
      $S$ si $w$ est la racine;
\item alors le type de $w$ est:
      $l_1 \ldep … \ldep l_k \ldep h \rdep r_1 \rdep … \rdep r_m$.
\stopitemize
Par exemple le mot \quote{était} dans la phrase ci-dessus gouverne un adverbe à
gauche et un adverbe à droite, et est subordonné en tant que copule. Sa catégorie
est donc $adv \ldep cop \rdep adv$.

Cette manière de catégoriser ne fonctionne que lorsque les dépendances sont
projectives. Pour intégrer les dépendances non-projectives, on utilise les
valences polarisés:

\startdefinition{Valences polarisées}
Pour chaque $d \in C$, on définit les valences polarisées
$\pln{d}, \plp{d}, \prn{d},$ et $\prp{d}$. Les valences $\plp{d}$ et $\prp{d}$ son
dites positives, et les valences $\pln{d}$ et $\prn{d}$ sont négatives. Deux
valences polarisées avec le même nom et qui pointent dans la même direction,
mais dont l'une est positive et l'autre négative sont {\bf duales}.
\stopdefinition

L'orientation gauche-droite de la flèche représente la direction de la
dépendance non-projective par rapport au mot. La flèche pointe vers le haut s'il
s'agit d'une dépendance où le mot est gouverneur (valence positive) et vers le
bas si le mot est subordonné par cette dépendance (valence négative).

\startdefinition{Catégories des GCD}
  Les catégories des GCD sont de la forme $B^P$, où $B$ est le {\bf type de base}
  qui correspond aux dépendances projectives, et $P$ est le {\bf potentiel},
  c'est-à-dire une suite de valences polarisées qui correspondent aux dépendances
  non-projectives.
\stopdefinition

Par exemple le mot \quote{citrouille} gouverne un déterminant à gauche et est
subordonné comme sujet, son type de base est donc $det \ldep suj$. De plus, il
gouverne une proposition subordonnée par une dépendance non-projective, donc son
potentiel est $\prp{acl}$. Le type complet est $\rppol{det \ldep suj}{acl}$.

Lorsqu'on déduit le type d'un groupe de mots, les potentiels sont concaténés.
Pour éliminer une valence du potentiel, il faut l'associer à sa valence
{\em duale}; il s'agit de la valence ayant le même nom de dépendance et la même
direction, mais de signe opposé. Cette association se fait selon la règle de la
{\em première disponible}: on élimine les valences dès qu'une association est
possible.

Lorsqu'un mot est subordonné par une dépendance non-projective $d$, il produit
une ancre:

\startdefinition{Type ancre}
  Soit $d \in C$ un nom de dépendance. Alors $\anc{\pln{d}}$ et $\anc{\prn{d}}$
  sont des {\em types ancres} qui sont des types de base de la GCD.
\stopdefinition

L'ancre apparait dans le type de base du mot, et sera neutralisé par une
dépendance projective avec le mot de la phrase qui accepte une ancre de ce type
(en pointillés sur le schéma). Par exemple le mot \quote{là} est subordonné à
\quote{citrouille} par une dépendance non-projective, il produit donc l'ancre
$\anc{\prn{acl}}$ une fois ses dépendances projectives satisfaites.  C'est le
mot \quote{apparue} qui accepte cette ancre à sa droite.

Il y a un dernier cas à considérer: celui des séquences
itérées\footcite{amblard_categorial_2016}. Certaines dépendances peuvent
apparaître un nombre illimité de fois de suite, comme les compléments
circonstanciels ou les énumérations. Dans ce cas, il n'est pas pratique d'ajouter
au lexique l'infinité de combinaisons possibles selon le nombre de fois où cette
dépendance apparait. Par exemple:

\placefigure[here,none]
{}
{
  \startnodes[dx=1em,dy=2.75ex,alternative=arrow]

    \lemma{ 0}    {Je}         {Je}
    \lemma{ 1.25} {te}         {te}
    \lemma{ 3.75} {donnerai}   {donnerai}
    \lemma{ 6.5}  {un}         {un}
    \cat  {10}    {frigidaire} {frigidaire,} {$det \ldep obj \rdep conj*$}
    \lemma{13.5}  {un2}        {un}
    \lemma{15.75} {joli}       {joli}
    \lemma{19}    {scooter}    {scooter,}
    \lemma{22}    {un3}        {un}
    \lemma{25}    {atomixer}   {atomixer,}
    \lemma{28}    {et}         {et}
    \lemma{29.5}  {du}         {du}
    \lemma{33}    {Dunlopillo} {Dunlopillo.}

    \depend{donnerai,frigidaire}   {obj}  {0.4}
    \depend{frigidaire,un}         {det}  {0.4}
    \depend{frigidaire,scooter}    {conj} {0.2}
    \depend{frigidaire,atomixer}   {conj} {0.2}
    \depend{frigidaire,Dunlopillo} {conj} {0.2}
  \stopnodes
}

Les séquences itérées sont résumées dans un type itéré:

\startdefinition{Type itéré}
  Soit $d \in C$ un nom de dépendance. Alors $d*$ est un type de base qui
  correspond à une dépendance qui peut apparaître un nombre indéterminé de fois
  (y compris zéro).
\stopdefinition

Les règles du calcul des types de dépendances sont relatives à la position du mot
dans la phrase. On note $i : B^P$ un mot à la position $i$ dont le type est
$B^P$. Le symbole $c$ dénote une dépendance projective de $C$, les symboles $P$,
$Q$ et $R$ dénotent des potentiels et le symbole $β$ dénote un type de base
quelconque.

\startformulas
  \startformula
    \frac{ i : c^P, \, j : \left[ c \ldep β \right]^Q }
         { j : β^{P,Q} }
    \mathbf{L^l}
  \stopformula

  \startformula
    \frac{ j : \left[ β \rdep c \right]^P, \, i : c^Q }
         { j : β^{P,Q} }
    \mathbf{L^r}
  \stopformula
\stopformulas

\startformulas
  \startformula
    \frac{ i : c^P, \, j : \left[ c^* \ldep β \right]^Q }
         { j : \left[ c^* \ldep β \right]^{P,Q} }
    \mathbf{I^l}
  \stopformula

  \startformula
    \frac{ j : \left[ β \rdep c^* \right]^P, \, i : c^P }
         { j : \left[ β \rdep c^* \right]^{P,Q} }
    \mathbf{I^r}
  \stopformula
\stopformulas

\startformulas
  \startformula
    \frac{ i : \left[ c^* \ldep β \right]^P }
         { i : β^P }
    \mathbf{Ω^l}
  \stopformula

  \startformula
    \frac{ i : \left[ β \rdep c^* \right]^P }
         { i : β^P }
    \mathbf{Ω^r}
  \stopformula
\stopformulas

\startformulas
  \startformula
    \frac{ i : β^{P,(l:↙v),Q,(k:↖v),R} }
         { i : β^{P,Q,R} }
    \mathbf{D^l}
  \stopformula

  \startformula
    \frac{ i : β^{P,(k:↗v),Q,(l:↘v),R} }
         { i : β^{P,Q,R} }
    \mathbf{D^r}
  \stopformula
\stopformulas

Les règles $\mathbf{D^l}$ et $\mathbf{D^r}$ s'appliquent à condition que
$k < l$, et que $Q$ ne contient pas de valence polarisée qui permettrait
d'appliquer la règle sur une distance plus courte.

Les règles $\mathbf{L^l}$ et $\mathbf{L^r}$ correspondent à l'application d'un
type fonction normal, les règles $\mathbf{I^l}$ et $\mathbf{I^r}$ permettent
l'application d'un type itéré à une suite d'arguments avec le même nom de
dépendances, et $\mathbf{Ω^{l}}$ et $\mathbf{Ω^{r}}$ s'applique lorsqu'un type
itéré n'a pas d'argument.

\section{Analyse syntaxique}

L'analyse syntaxique avec les CDG se fait grâce à l'algorithme {\em CDG
Analyst}\footcite{dekhtyar_categorial_2015}. Pour analyser le potentiel d'un
type, il faut distinguer les valences censées se trouver à gauche d'une paire de
valences duales, et celles qui se trouvent à droite. Les valences de forme $↙d$
et $↗d$ sont les \quote{parenthèses ouvrantes} (à gauche) de ces paires et
celles de forme $↖d$ et $↘d$ sont les \quote{parenthèses fermantes}. Si $v$ est
une parenthèse ouvrante, on note $\bar{v}$ sa valence duale, qui est une
parenthèse fermante.  On note $V^l$ l'ensemble des parenthèses ouvrantes et
$V^r$ l'ensemble des parenthèses fermantes.

\startdefinition{Déficit de valences}
Soit $P$ est une suite de valences polarisées; on note $|P|_v$ le nombre
d'occurrences de $v$ dans $P$. Soit $v \in V^l$, on définit de la façon
suivante les déficits de valences dans $P$:

\startformula
  Δ_v(P) = \max\big\{|P'|_v - |P'|_{\bar{v}} \; \big\| \; P' \text{ est un suffixe de } P\big\}
\stopformula

\startformula
Δ_{\bar{v}}(P) = \max\big\{|P'|_{\bar{v}} - |P'|_v \; \big\| \; P' \text{ est un préfixe de } P\big\}
\stopformula
\stopdefinition

La proposition suivante nous donne une condition d'échec du parenthésage:

\startdefinition{Fonctions d'échec}
  Soit une phrase $w = w_{1}…w_{n}$, soit $α \in V^{l}$ et $1 ≤ i ≤ n$. On
  définit les fonctions d'échec à droite et à gauche de la façon suivante:

  \startformula
    π^L(α, i) = \max \big\{ Δ_α(P) \;\big\|\; B^P \in λ(w_1…w_i) \big\}
  \stopformula

  \startformula
    π^L(\bar{α}, i) = \max \big\{ Δ_{\bar{α}}(P) \;\big\|\; B^P \in λ(w_{n-i+1}…w_n) \big\}
  \stopformula

  Ces fonctions peuvent être calculées incrémentalement de la façon suivante:

  \startformula
    π^L(α, i+1) = \max \big\{
      Δ_α(P) + \max \{ 0, π^L(α, i) - Δ_{\bar{α}}(P) \}
      \;\big\|\;
      B^P \in λ(w_{i+1})
    \big\}
  \stopformula

  \startformula
    π^R(\bar{α}, i+1) = \max \big\{
      Δ_{\bar{α}}(P) + \max \{ 0, π^R(\bar{α}, i) - Δ_{α}(P) \}
      \;\big\|\;
      B^P \in λ(w_{n-i+1})
    \big\}
  \stopformula
\stopdefinition

\startproposition{Condition d'échec}
  Si $B^{P} ⊦ S$ pour $B^{P} = b_{1}^{p_{1}}…b_{n}^{p_{n}} \in λ(w)$,
  alors:
  \startformula
    Δ_α(p_i…p_j) ≤ π^R(\bar{α}, n-j)
  \stopformula
  et
  \startformula
    Δ_{\bar{α}}(p_i…p_j) ≤ π^L(α, i-1)
  \stopformula
  pour tout $1 ≤ i ≤ j ≤ n$, et pour tout $α \in V^{l}$.
\stopproposition

Soit $w = w_{1}…w_{n}$ une phrase. L'algorithme {\em CDG Analyst} remplit une
matrice triangulaire de taille $n \times n$ où chaque cellule $M\[i,j\]$
($i \le j$) correspond à une sous-chaîne $w_{i}…w_{j}$ de $w$. On définit un
vecteur $(v_{1},…,v_{p})$ correspondant à un ordre quelconque de $V^{l}$, avec
le vecteur correspondant de valences duales $(\bar{v}_{1},…,\bar{v}_{p})$.  Une
cellule reçoit une liste d'items de la forme $I = <B,Δ^{L},Δ^{R},I^{l},I^{r}>$
où:

\startitemize
  \item $B$ est une catégorie de base associée à la sous-chaîne,
  \item $Δ^{L} = (Δ_{v_{1}},…,Δ_{v_{p}})$ et
    $Δ^{R} = (Δ_{\bar{v}_{1}},…,Δ_{v_{p}})$ sont des vecteurs d'entiers
    contenants les déficits de valences du potentiel,
  \item $I^{l}$ et $I^{r}$ sont les items à partir desquels l'item courant est
    calculé (c'est-à-dire en concaténant les sous-chaînes correspondant à
    $I^{l}$ et $I^{r}$)
\stopitemize

\startlines
{\bf cdg_analyst($w$)}
{\bf Entrée: } une séquence de mots $w = w_{1} … w_{n}$
{\bf Sortie: } Oui($I$) ssi $w \in L(G)$; Non sinon
\startlinenumbering[algorithm]
  $π_{L} \gets$ calc_fail_func_l($w$)
  $π_{R} \gets$ calc_fail_func_r($w$)
  {\bf Pour chaque} $k \in \{1..n\}$
  \quad propose($k$)
  {\bf Fin Pour}
  {\bf Pour chaque} $l \in \{2..n\}$
  \quad {\bf Pour chaque} $i \in \{1..n-l\}$
  \quad \quad $j \gets i + l - 1$
  \quad \quad {\bf Pour chaque} $k \in \{i..j-1\}$
  \quad \quad \quad subordinate_l($i$, $k$, $j$)
  \quad \quad \quad subordinate_r($i$, $k$, $j$)
  \quad \quad {\bf Fin Pour}
  \quad {\bf Fin Pour}
  {\bf Fin Pour}
  {\bf Si} $I = \<S, \vec{0}, \vec{0}, I^{l}, I^{r}\>$
  \ \ {\bf Alors retourner} Oui($I$)
  \ \ {\bf Sinon retourner} Non
\stoplinenumbering
\stoplines

L'algorithme comence par calculer et stocker les valeurs des fonctions d'échec
$π^{L}(α,i)$ et $π^{R}(\bar{α},i)$ dans les matrices $π_{L}$ et $π_{R}$ de
dimension $n \times |Δ^{L}|$.

\startlines
{\bf calc_fail_fun_l($w$)}
{\bf Entrée: } une séquence de mots $w = w_{1} … w_{n}$
{\bf Sortie: } une matrice $π_L$
\startlinenumbering[algorithm]
  {\bf Pour chaque} $v \in V^{l}$
  \quad $π^L\[v,0\] \gets 0$
  \quad {\bf Pour chaque} $i \in {1..n}$
  \quad \quad $π_{max} \gets 0$
  \quad \quad {\bf Pour chaque} $B^{P} \in λ(w_{i})$
  \quad \quad \quad $π_{max} \gets \max\{π_{max}, Δ_{v}(P) + \max\{π^{L}\[v,i-1\] - Δ_{\bar{v}}(P), 0\}\}  $
  \quad \quad {\bf Fin Pour}
  \quad \quad $π^{L}\[v,i\] \gets π_{max}$
  \quad {\bf Fin Pour}
  {\bf Fin Pour}
\stoplinenumbering
\stoplines

\startlines
{\bf calc_fail_fun_r($w$)}
{\bf Entrée: } une séquence de mots $w = w_{1} … w_{n}$
{\bf Sortie: } une matrice $π_L$
\startlinenumbering[algorithm]
  {\bf Pour chaque} $\bar{v} \in V^{r}$
  \quad $π^L\[v,0\] \gets 0$
  \quad {\bf Pour chaque} $i \in {1..n}$
  \quad \quad $π_{max} \gets 0$
  \quad \quad {\bf Pour chaque} $B^{P} \in λ(w_{n-i+1})$
  \quad \quad \quad $π_{max} \gets \max\{π_{max}, Δ_{\bar{v}}(P) + \max\{π^{R}\[v,i-1\] - Δ_{v}(P), 0\}\}  $
  \quad \quad {\bf Fin Pour}
  \quad \quad $π^{R}\[v,i\] \gets π_{max}$
  \quad {\bf Fin Pour}
  {\bf Fin Pour}
\stoplinenumbering
\stoplines

Une fois les fonctions d'échec calculées, l'algorithme remplit les cases
diagonales $M\[i,i\]$ pour $1 \lt i \lt n$. Pour cela, il parcours les types
possibles de la forme $B^{P}$ dans le lexique $λ(w_{i})$, et vérifie que le type
proposé n'est pas exclu par la proposition 1 grâce à la fonction d'échec.

\startlines
{\bf propose($i$)}
{\bf Entrée: } une cellule $M\[i\]$ de la matrice $M$
\startlinenumbering[algorithm]
  {\bf Pour tout} $B^{P} \in λ(w_{i})$
  \quad good_c ← {\em true}
  \quad {\bf Pour tout} $v \in V^{l}$
  \quad \quad $Δ^{L}\[v\] ← Δ_{v}(P)$
  \quad \quad $Δ^{R}\[\bar{v}\] ← Δ_{\bar{v}}(P)$
  \quad \quad {\bf Si} $Δ^{L}\[v\] > π^{R}\[\bar{v}, n-i\]$ {\bf ou} $Δ^{R}\[\bar{v}\] > π^{L}\[v, i-1\]$
  \quad \quad \ \ {\bf Alors} good_c ← {\em false}
  \quad {\bf Fin Pour}
  \quad {\bf Si} good_c {\bf Alors} add_item($i,i,<B,Δ^L,Δ^R,∅,∅>$)
  {\bf Fin Pour}
\stoplinenumbering
\stoplines

\startlines
{\bf add_item($i,j,I$)}
{\bf Entrée: } une cellule $M\[i,j\]$ de la matrice $M$ et un item $I = <B,Δ^{L},Δ^{R},I^{l},I^{r}>$
\startlinenumbering[algorithm]
  $M\[i,j\] \gets M\[i,j\] ∪ \{I\}$
  {\bf Si} $B = [C'^{*} \ldep β]$
  \ \ {\bf Alors} add_item($i$, $j$, $<[β],Δ^L,Δ^R,I^l,I^r>$)
  {\bf Si} $B = [β \rdep C'^{*} ]$
  \ \ {\bf Alors} add_item($i$, $j$, $<[β],Δ^L,Δ^R,I^l,I^r>$)
\stoplinenumbering
\stoplines

Finalement, on parcours les cases de la matrice $M$ diagonale par diagonale.
Pour chaque case correspondant à une sous-chaîne de mots, on parcours les façons
possible de diviser cette sous-chaîne en deux sous-sous-chaînes de taille
inférieures. Les items pour les sous-sous-chaînes ayant été calculés
précedemment, on utilise les fonctions {\bf subordinate_l} et
{\bf subordinate_r} pour produire les items de la sous-chaîne. Ces fonctions
vérifient que l'enchaînement proposé respecte la proposition 1, puis calculent
le type de la sous-chaîne si ceux des sous-sous-chaînes sont compatibles.

\startlines
{\bf subordinate_l($i,k,j$)}
{\bf Entrée: } nombres entiers $i, k, j$
\startlinenumbering[algorithm]
  {\bf Pour tout} $I_{1} = <α_{1},Δ^{L}_{1},Δ^{R}_{1},I^{l}_{1},I^{r}_{1}> \in M\[i,k\],$
  \\{\bf et} $I_{2} = <α_{2},Δ^{L}_{2},Δ^{R}_{2},I^{l}_{2},I^{r}_{2}> \in M\[k+1,j\]$
  \quad good_items ← {\em true}
  \quad {\bf Pour tout} $v \in V^{l}$
  \quad \quad $Δ^{L}\[v\] ← Δ^{L}_{2}(v) + \max \{ Δ^{L}_{1}(v) - Δ^{R}_{2}\(v\), 0 \}$
  \quad \quad $Δ^{R}\[\bar{v}\] ← Δ^{L}_{2}(\bar{v}) + \max \{ Δ^{L}_{1}(\bar{v}) - Δ^{R}_{2}\(\bar{v}\), 0 \}$
  \quad \quad {\bf Si} $Δ^{L}\[v\] > π^{R}\[\bar{v}, n-i\]$ {\bf ou} $Δ^{R}\[\bar{v}\] > π^{L}\[v, i-1\]$
  \quad \quad \ \ {\bf Alors} good_items ← {\em false}
  \quad {\bf Fin Pour}
  \quad {\bf Si} good_items {\bf Alors}
  \quad \quad {\bf Si} $α_{1} = B$ {\bf et} $α_{2}=\[B \ldep β\]$
  \quad \quad \ \ {\bf Alors} add_item(i, j, $<[β], Δ^L, Δ^R, I^1, I^2>$)
  \quad \quad {\bf Sinon, si} $α_{1} = B$ {\bf et} $α_{2} = [B^*\ldep β]$ {\bf ou} $α_{1} = [ε]$
  \quad \quad \ \ {\bf Alors} add_item(i, j, $<α_{2}, Δ^L, Δ^R, I^1, I^2>$)
  {\bf Fin Pour}
\stoplinenumbering
\stoplines

La fonction {\bf subordinate_r} est définie de façon similaire.

\section{Apprentissage}

L'apprenabilité d'une grammaire est définie de la
façon suivante\footcite{gold_language_1967}:

\startdefinition{Équivalence entre grammaires}
Soit $G$ une grammaire de dépendances. On note $L(G)$ l'ensemble des phrases
reconnues par $G$ et $D(G)$ l'ensemble des structures de dépendances que $G$
peut associer à une phrase.

On dit que deux grammaires $G$ et $G'$ sont {\bf faiblement} équivalentes si
$L(G) = L(G')$.

On dit que deux grammaires $G$ et $G'$ sont {\bf fortement} équivalentes si
elles sont faiblement équivalentes et $D(G)$ et $D(G')$ sont en bijection.
\stopdefinition

\startdefinition{Apprentissage à la limite}
Soit $Φ(G)$ un ensemble d'exemples de $G$ (des phrases de $L(G)$ ou des
structures de dépendances de $D(G)$) et $σ$ une séquence de $Φ(G)$.

Soit $A$ un algorithme qui pour chaque sous-séquence $σ\left[1…i\right]$ d'une
séquence $σ$ de $Φ(G)$ calcule une GCD $A(σ\left[1…i\right])$.

$A$ apprend une grammaire $G$ si pour toute séquence $σ$, il existe un entier
$T>0$ tel que $\forall t>T, A(σ\left[1…t\right])$ est fortement équivalent à
$G$.

Une grammaire $G$ est apprenable s'il existe un algorithme qui l'apprend à la
limite.
\stopdefinition

\startdefinition{Généralisation $K$-étoile}
Soit un entier $K>1$. On définit une CDG $𝒞^K(G)$, la généralisation $K$-étoile
de $G$ de la manière suivante: pour chaque mot $w$ ayant une catégorie
$t \in λ(w)$ de la forme $L \ldep t_1 \ldep … \ldep t_k \ldep M \ldep h \rdep R$
on ajoute à $λ(w)$ les catégories $L \ldep M \ldep h \rdep R$ et
$L \ldep d^* \ldep M \ldep h \rdep R$. On fait de même lorsque la séquence $t_1,…,t_k$
apparaît à droite.
\stopdefinition

\startdefinition{Grammaire $K$-étoile révélatrice}
Une CDG $G$ est $K$-étoile révélatrice si $G=𝒞(G)$.

On note $CDG^{K→*}$ la classe des grammaires $K$-étoile révélatrices.
\stopdefinition

On calcule le type correspondant à un mot de la façon suivante:

\startdefinition{Vicinity}
Soit $D = (W, E)$ une structure de dépendances et $w \in W$ un mot qui a:
\startitemize
\item une dépendance nommée $h$ qui arrive sur $w$ (ou la racine $s$),
\item une séquence de dépendances projectives à gauche $l_1,…,l_k$,
\item une séquence de dépendances projectives à droite $r_1,…,r_m$,
\item une séquence de dépendances non-projectives $d_1,…,d_n$ avec les
      polarités respectives $p_1,…,p_n$.
\stopitemize

Lorsque la dépendance $h$ est projective, la {\em vicinity} de $w$ dans $D$ est
le type:
\startformula
\left[ l_1 \ldep … \ldep l_k \ldep h \rdep r_1 \rdep … \rdep r_m \right]^P
\stopformula
où $P$ est une permutation de $p_1d_1,…,p_nd_n$ dans un ordre lexicographique
compatible avec l'ordre des polarités: $↖ < ↘ < ↙ < ↗$.

Lorsque la dépendance $h$ est non-projective, la {\em vicinity} de $w$ dans $D$
est:
\startformula
\left[ l_1 \ldep … \ldep l_k \ldep \anc{v} \rdep r_1 \rdep … \rdep r_m \right]^P
\stopformula
où $v$ est une valence polarisé dont le nom de dépendance est $h$, la direction
est celle de $h$ et la polarité est négative.
\stopdefinition

Le problème de cette définition est qu'elle ne permet pas de déterminer quel mot
de la phrase a l'ancre comme dépendance. Pour implémenter l'algorithme, j'ai
utilisé la définition suivante:

\startdefinition{Dépendance ancre}
  Soit $w \in W$ un mot de la phrase gouverné par une dépendance $d$. Soit
  $r→h_{k}→…→h_{1}→w$ le chemin de dépendances de la racine $r$ à $w$. Soit
  $h_{i}$ le premier mot dominant $w$ (de $w$ à $r$) tel que la dépendance
  $h_{i}→w$ est projective lorsqu'on la rajoute à la structure de dépendances de
  la phrase. (la dépendance $r→w$ est forcément projective puisque $r$ domine
  toute la phrase.)

  Si $d$ n'est pas projective, on rajoute à la phrase une dépendance $h_{i}→w$
  labellisée par l'ancre $\# v$ produite par le mot $w$.
\stopdefinition

Les grammaires $K$-étoile révélatrices sont apprenables à la limite à partir de
structures de dépendances. Comme on ne peut pas en pratique s'assurer que les
exemples donnés en entrée correspondent bien à une grammaire $K$-étoile
révélatrice, on définit une sous-classe appelée $K$-étoile simple:

\startdefinition{Grammaire $K$-étoile simple}
Soit un entier $K>1$, soit $t$ une catégorie et $d$ le nom d'une dépendance. $t$
est $K$-étoile simple à gauche sur $d$ si pour toute occurrence
$l_1 \ldep … \ldep l_k$ avec $l_i=d$ ou $l_i=x^*$, il y a au plus $K-1$
occurrences de $d$, et aucune occurrence de $d$ s'il y a une occurrence de $d^*$.

On définit $K$-étoile simple à droite de façon similaire. $t$ est $K$-étoile
simple s'il est $K$-étoile simple à droite et à gauche.

Une grammaire $G$ est $K$-étoile simple si toutes ses catégories sont $K$-étoile
simple.

On note $CDG^{K\lettertilde *}$ la classe des langages $K$-étoile simples.
\stopdefinition

Une alternative à la classe des grammaires $K$-étoile simples est la classe des
grammaires $K$-étoile globales

\startdefinition{Grammaire $K$-étoile globale}
Soit un entier $K>1$, soit $t$ une catégorie et $d$ le nom d'une dépendance. $t$
est $K$-étoile globale à gauche sur $d$ si pour toute sous-suite
$l_1 \ldep … \ldep l_k$ à gauche, il y a au plus $K-1$ occurrences de $d$, et
aucune occurrence de $d$ s'il y a une occurrence de $d^*$.

On définit $K$-étoile globale à droite de façon similaire. $t$ est $K$-étoile
globale s'il est $K$-étoile globale à droite et à gauche.

Une grammaire $G$ est $K$-étoile globale si toutes ses catégories sont $K$-étoile
globale.

On note $CDG^{K\lettertilde_{g} *}$ la classe des langages $K$-étoile globale.
\stopdefinition

L'algorithme $TGE^{(K)}$ apprend à la limite les grammaires $K$-étoile simples
de façon incrémentale, dans le sens suivant:

\startdefinition{Apprentissage incrémental}
L'ensemble $Δ(G)$ est monotone par rapport à un ordre partiel $≼$ si pour toute
grammaires $G_1$ et $G_2$, $G_1 ≼ G_2 \implies Δ(G_1) ⊆ Δ(G_2)$. Soit $≼$ un
tel ordre.

Soit $A$ un algorithme d'inférence à partir de structures de dépendances et $σ$
une séquence d'exemples pour une grammaire $G$.

\startitemize
\item $A$ est monotone si $A(σ\left[1…i\right]) ≼ A(σ\left[1…j\right])$ pour tout
      $i$ et $j$..
\item $A$ est fidèle si $Δ(A(σ\left[1…i\right])) ⊆ Δ(G)$ pour tout $i$.
\item $A$ est expansif si $σ\left[1…i\right] ⊆ Δ(A(σ\left[1…i\right]))$ pour tout $i$.
\stopitemize

Un algorithme d'apprentissage est incrémental s'il est monotone, fidèle et
expansif.
\stopdefinition

\startlines
{\bf $TGE^{(k)}$}
{\bf Entrée: } une séquence σ de structures de dépendances
{\bf Sortie: } une grammaire $K$-étoile révélatrice $G$
\startlinenumbering[algorithm]
G \gets (W, C, V, λ)
\quad avec W = ∅, C = \{s\}, V = ∅ et λ : w \mapsto ∅
{\bf Pour chaque} $D = (W,E) \in σ$
\quad {\bf Pour chaque} $w \in W$
\quad \quad $W \gets W ∪ \{ w \}$
\quad \quad $t_w \gets V(w, D)$ la proximité de $w$ dans $D$

\quad \quad {\bf Tant que} $t_w = \left[ α \ldep d \ldep … \ldep d \ldep β \right]^P$
\quad \quad \quad $t_w \gets \left[ α \ldep d^* \ldep β \right]^P$
\quad \quad {\bf Fin Tant que}

\quad \quad {\bf Tant que} $t_w = \left[ α \rdep d \rdep … \rdep d \rdep β \right]^P$
\quad \quad \quad $t_w \gets \left[ α \rdep d^* \rdep β \right]^P$
\quad \quad {\bf Fin Tant que}

\quad \quad $λ(w) \gets λ(w) ∪ \{t_w\}$
\quad {\bf Fin Pour}
{\bf Fin Pour}
\stoplinenumbering
\stoplines
