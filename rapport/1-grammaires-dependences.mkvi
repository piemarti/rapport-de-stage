\chapter{Grammaires de dépendances} % ===========================================

Les dépendances sont un formalisme permettant de représenter la structure
syntaxique d'une phrase. Cet outil a notamment été développé par Lucien Tesnière
dans \titleyear{tesniere_elements_1976}. L'analyse en dépendances d'une phrase
consiste à tracer les liens qui relient les différentes unités considérées (des
mots, le plus souvent, mais aussi des morphèmes, groupes de mots, etc.) de la
phrase entre elles. Cette méthode propose une vision de la phrase alternative
par rapport à l'analyse en constituents, qui est le paradigme le plus répandu,
et qui structure la phrase par une imbrication de groupes de mots: groupe
nominal, groupe verbal, complément, proposition, …

Dans une première section je présenterai les principes généraux de l'analyse en
dépendances. Puis dans la deuxième section, je parlerai des grammaires formelles
en dépendances ainsi que des problèmes d'expressivité et de complexité.
Finalement, dans la troisième section, je présenterai des applications de
l'analyse en dépendances.

\section{Principes de base} % ---------------------------------------------------

Il existe plusieurs théories basées sur les dépendances, qui utilisent cette
notion de façon différente les unes des autres.  On peut trouver une revue
détaillée de l'approche par dépendances pour l'analyse syntaxique dans
\cite[title][nivre_dependency_2005] \cite[authoryear][nivre_dependency_2005].

\subsection{La relation de dépendance}

D'après Mel'čuk\footcite{melcuk_dependency_1987}, la structure de la phrase est
exprimée naturellement par des caractéristiques {\em lexicales} (les mots
eux-mêmes) et des caractéristiques {\em non lexicales}: l'ordre des mots, la
prosodie et les inflexions (accords, conjugaison, etc.). Ces caractéristiques
non lexicales transmettent l'information syntaxique de façon implicite et
détournée, c'est pourquoi il faut trouver un moyen plus explicite de représenter
la structure de la phrase. Cette structure doit exprimer les relations entre les
mots de la phrase: tel mot vient avant tel autre, est prononcé plus rapidement
que tel autre, est en accord avec tel autre, etc. Dans les grammaires de
dépendances, cela est exprimé par la relation de dépendance.

Une dépendance est une relation binaire qui relie deux mots d'une phrase.  L'un
des mots est appelé {\bf régissant} et l'autre {\bf subordonné}.  On note
$h → d$ la présence d'une dépendance entre le régissant $h$ et le subordonné
$d$. On note $h \transto d$ la fermeture transitive de $→$, c'est-à-dire la
présence d'un chemin de dépendances $h → … → d$, et on dit que $h$ {\bf domine}
$d$ lorsque $h \transto d$.  Analyser une phrase consiste à tracer les relations
de dépendance entre ses mots; par exemple l'analyse de la phrase \quote{Ici,
tout est tranquille.} pourrait donner:

\placefigure[here,none]{}
{
  \startnodes[dx=4.5em,alternative=arrow]
    \placenode[0,0][reference=ici]{\node{\it Ici}}
    \placenode[0.75,0][reference=tout]{\node{\it tout}}
    \placenode[1.5,0][reference=est]{\node{\it est}}
    \placenode[0.75,1.25][reference=tranquille]{\node{\it tranquille}}

    \connectnodes [tranquille,ici]
    \connectnodes [tranquille,tout]
    \connectnodes [tranquille,est]
  \stopnodes
}

Les relations peuvent recevoir une étiquette qui permet de distinguer différents
types de relations, par exemple:

\placefigure[here,none]{}
{
  \startnodes[dx=4.5em,alternative=arrow]
    \placenode[0,0][reference=ici]{\node{\it Ici}}
    \placenode[0.75,0][reference=tout]{\node{\it tout}}
    \placenode[1.5,0][reference=est]{\node{\it est}}
    \placenode[0.75,2][reference=tranquille]{\node{\it tranquille}}

    \connectnodes [tranquille,ici] [label={\tt\tfx advmod}, offset=-0.25]
    \connectnodes [tranquille,tout] [label={\tt\tfx nsubj}]
    \connectnodes [tranquille,est] [label={\tt\tfx cop}, offset=0.25]
  \stopnodes
}

Dans la plupart des grammaires de dépendances, la relation de dépendance a les
propriétés suivantes:

\startitemize
  \item Antisymétrie: si $h → d$ alors $d ↛ h$,
  \item Antiréflexivité: $d ↛ d$,
  \item Antitransitivité: si $h → d$ et $d → e$ alors $h ↛ e$,
\stopitemize

La structure de la phrase devient alors un graphe orienté acyclique. Pour que
l'on considère l'analyse complète, ce graphe doit également être connexe.
Ainsi, chaque mot a au plus un régissant, et exactement un mot n'a pas de
régissant et domine tous les autres: c'est la racine.

\subsection{Projectivité}

La structure des dépendances forme l'{\bf ordre structural} de la phrase.
Cependant, lorsqu'on parle ou qu'on écrit, on doit produire une séquence
ordonnée de mots: c'est l'{\bf ordre linéaire}. La projectivité est une
propriété issue de la relation entre ordre structural et ordre linéaire.

On appelle {\bf projection} d'un mot $h$ l'ensemble des mots qu'il domine :
$\{d ~\|~ h \transto d\}$. Un mot est {\bf projectif} si sa projection est une
séquence continue dans l'ordre linéaire. La phrase est dite projective si tous
ses mots sont projectifs.

\placefigure[here]
{Une phrase non-projective (en rouge, la projection de \quotation{ça})}
{
  \startnodes[dx=4.5em,alternative=arrow]
    \lemmap{0} {Les}     {Les}
    \lemmap{1} {homards} {homards,}
    \lemma {2} {je}      {je}
    \lemma {3} {n}       {n'}
    \lemma {4} {aime}    {aime} \root{4}
    \lemma {5} {pas}     {pas}
    \lemmap{6} {ca}      {ça.}

    \rootis{aime}

    \depend {homards, Les}     {det}    {-0.4}
    \depend {aime,    je}      {nsubj}  {-0.4}
    \depend {aime,    n}       {advmod} { 0.35}
    \depend {aime,    pas}     {advmod} {-0.35}
    \depend {aime,    ca}      {obj}    { 0.4}
    \depend {ca,      homards} {appos}  {-0.4}
  \stopnodes
}

Bien que les phrases non-projectives ne soient pas la norme, elles existent bel
et bien dans les langues naturelles. Comme le montre la phrase ci-dessus, la
thématisation, qui consiste à placer le thème en début de phrase, peut aboutir à
une structure non-projective. Certaines langues où l'ordre des mots dans la
phrase n'est pas fixé permettent de produire facilement des constructions
non-projectives. Il faut donc prendre en compte la non-projectivité, ce qui pose
certains problèmes comme on le verra par la suite.

\section{Dépendances et grammaires formelles} %----------------------------------

\subsection{Exemple: la grammaire de Hays-Gaifman}

La grammaire de Hays-Gaifman\footcite{gaifman_dependency_1965} est la première
grammaire formelle basée sur les dépendances. Elle est définie par un ensemble
de catégories de mots et trois ensembles de règles:
\startitemize
\item Un ensemble de règles $L_{I}$ qui donne pour chaque catégorie $X$ un
  ensemble de règles de la forme $X(Y_{1},…,Y_{l},*,Y_{l+1},…,Y_{n})$. Cela
  signifie qu'un mot $w$ de la catégorie $X$ peut gouverner une suite de mots
  des catégories $Y_{1},…,Y_{n}$ placés dans l'ordre linéaire donné par la règle
  (avec $w$ à la place de $*$).

\item Un ensemble de règles $L_{II}$ qui donne pour chaque catégorie la liste
  des mots qui lui appartiennent.

\item Une règle $L_{III}$ qui donne la liste des catégories dont l'occurrence
  peut régir une phrase entière.
\stopitemize

Exemple:

\startformula
L_{I}: \{ Det(*); Subj(Det,*); Verb(Subj,*,Obl); Case(*); Obl(Case,Det,*)\}
\stopformula

\startformula
L_{II}: \{ Det \mapsto \{le\}; Subj \mapsto \{moineau\}; Verb \mapsto \{vole\};
Case \mapsto \{vers\}; Obl \mapsto \{sud\}
\}
\stopformula
\startformula
L_{III}: \{ Verb \}
\stopformula

\placefigure[here,none]
{}
{
  \startnodes[dx=4.5em,alternative=arrow]
    \lemma {0} {Le}      {Le}
    \lemma {1} {moineau} {moineau}
    \lemma {2} {vole}    {vole}
    \lemma {3} {vers}    {vers}
    \lemma {4} {le}      {le}
    \lemma {5} {sud}     {sud.}

    \depend {moineau,Le}   {det}   {-0.4}
    \depend {vole,moineau} {subj} {-0.5}
    \depend {vole,sud}     {obl}   { 0.36}
    \depend {sud,le}       {det}   {-0.4}
    \depend {sud,vers}     {case}  {-0.35}
  \stopnodes
}

\startdefinition{Langage généré}
  Le langage généré par une grammaire $G = (L_{I},L_{II},L_{III})$ est
  l'ensemble $L(G)$ des phrases reconnues par $G$. Le langage des structures de
  dépendances générées par $G$ est l'ensemble $Δ(G)$ des structures de
  dépendances issues de l'analyse syntaxique avec $G$.
\stopdefinition

On peut remarquer que les règles de dépendances sont données en termes de
l'ordre linéaire dans une séquence continue autour du mot. Le langage des
structures de dépendances $Δ(G)$ ne contient donc pas de structure
non-projective.

\subsection{Expressivité et complexité}

La grammaire de Hays-Gaifman est faiblement équivalentes aux grammaires
hors-contexte: tout langage reconnu par une grammaire hors-contexte peut être
reconnu par une grammaire de Hays-Gaifman; cependant les structures générées par
celle-ci sont moins riche. Il existe un algorithme qui effectue l'analyse
syntaxique avec la grammaire de Hays-Gaifman en temps $O(n^{3})$.

Pour améliorer l'expressivité, il faudrait prendre en compte les dépendances
non-projectives. Malheureusement, le problème de l'analyse syntaxique avec
non-projectivité est $NP$-complet\footcite{neuhaus_complexity_1997}.

Cependant, il semble que les langues naturelles ne produisent pas des phrases
arbitrairement non-projectives. Non seulement celles-ci apparaissent rarement,
mais elles sont limitées par certaines caractéristiques comme la longueur des
discontinuités dans la projection\footcite{yadav_are_2019}. Il est donc
intéressant de chercher des grammaires de dépendances plus expressives grâce à
des restrictions plus faibles sur les phrases
projectives\footcite{gomez-rodriguez_restricted_2016}. Les grammaires
catégorielles de dépendances, présentées dans la partie 2, sont une réponse
possible à ce problème.
