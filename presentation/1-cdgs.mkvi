\chapter{Apprendre les grammaires catégorielles de dépendances} % ===============

Le stage devra donner lieu à l'implémentation d'un algorithme d'apprentissage
pour les grammaires catégorielles de dépendances. Dans cette partie, après avoir
introduit les grammaires de dépendances puis les grammaires catégorielles, je
présenterai les grammaires catégorielles de dépendances avant de conclure sur un
aperçu du travail à réaliser.

\section{Grammaires de dépendances} % -------------------------------------------

Les grammaires formelles les plus répandues sont les grammaires de constituants,
qui analysent la phrase comme une imbrication de parties de discours dans
lesquelles sont répartis les mots. Les grammaires de dépendances fournissent une
autre manière de voir la phrase, qui se focalise sur les relations qui existent
entre les unités lexicales. On peut trouver une revue détaillée de cette approche
dans \cite[title][nivre_dependency_2005]
\cite[authoryear][nivre_dependency_2005].

\subsection{Dépendances}

\placefigure[here]
{Une phrase avec sa structure de dépendances}
{
  \startnodes[dx=4.5em,alternative=arrow]
    \lemma {0} {Le}      {Le}
    \lemma {1} {moineau} {moineau}
    \lemma {2} {vole}    {vole}    \root{2}
    \lemma {3} {vers}    {vers}
    \lemma {4} {le}      {le}
    \lemma {5} {sud}     {sud}
    \lemma {6} {en}      {en}
    \lemma {7} {hiver}   {hiver.}

    \rootis{vole}
  
    \depend {moineau,Le}   {det}   {-0.4}
    \depend {vole,moineau} {nsubj} {-0.5}
    \depend {vole,sud}     {obl}   { 0.36}
    \depend {vole,hiver}   {obl}   { 0.35}
    \depend {sud,le}       {det}   {-0.4}
    \depend {sud,vers}     {case}  {-0.35}
    \depend {hiver,en}     {case}  {-0.4}
  \stopnodes
}

La dépendance d'une grammaire de dépendances est une relation binaire
antisymmétrique entre les formes lexicales de la phrase. Dans une telle relation,
par exemple $h \to d$, on dit que $h$ est le régissant et $d$ le subordonné. On
peut prendre la fermeture transitive $\transto$ de $\to$; on dit que $h$ domine
$d$ lorsque $h \transto d$. On peut associer une étiquette à chaque dépendance;
par exemple {\tt det} pour la dépendance du déterminant au nom, ou {\tt nsubj}
pour la dépendance de l'objet au verbe.

Le but d'une grammaire de dépendances est d'associer une structure de dépendances
à la phrase à analyser. Pour cela, plusieurs types de critères peuvent être
utilisés, qu'ils soient sémantiques (par exemple: le prédicat gouverne ses
arguments), ou bien syntaxique (par exemple: $h$ gouverne $d$ si $d$ s'accorde
avec $h$).

Dans la plupart des cas, la structure de dépendances doit former un arbre
orienté. Chaque mot a au plus un gouverneur, il n'y a pas de boucle de
dépendance, et un mot sans gouvernant (la racine) domine tous les autres mots de
la phrase.

Cette façon d'analyser la phrase se prête particulièrement bien à l'analyse
sémantique. En effet, les relations de dépendances sont souvent proches des
relations sémantiques entre les mots (comme la relation prédicat-argument).
La théorie Sens-Texte\footcite{kahane_grammaires_2001}, par exemple, utilise les
grammaires de dépendances comme représentation intermédiaire dans une
correspondance entre sens et texte.

\subsection{Projectivité et expressivité}

La projection d'un mot dans une phrase avec une structure de dépendances est
l'ensemble des mots qu'il domine. On dit qu'une phrase est projective si pour
tout mot, sa projection est un segment continu de la phrase.

\placefigure[here]
{Une phrase non-projective (en rouge, la projection de \quotation{ça})}
{
  \startnodes[dx=4.5em,alternative=arrow]
    \lemmap{0} {Les}     {Les}
    \lemmap{1} {homards} {homards,}
    \lemma {2} {je}      {je}
    \lemma {3} {n}       {n'}
    \lemma {4} {aime}    {aime} \root{4}
    \lemma {5} {pas}     {pas}
    \lemmap{6} {ca}      {ça.}
  
    \rootis{aime}

    \depend {homards, Les}     {det}    {-0.4}
    \depend {aime,    je}      {nsubj}  {-0.4}
    \depend {aime,    n}       {advmod} { 0.36}
    \depend {aime,    pas}     {advmod} {-0.35}
    \depend {aime,    ca}      {obj}    { 0.4}
    \depend {ca,      homards} {appos}  {-0.4}
  \stopnodes
}

Dans \cite[title][gaifman_dependency_1965],
\cite[authoryears][gaifman_dependency_1965] a montré que les grammaires de
dépendances qui ne produisent que des phrases projectives sont faiblement
équivalentes à la grammaire de Chomsky (elles définissent les mêmes langages mais
produisent des structures moins riches). En d'autre termes, les grammaire de
dépendances projectives sont des grammaires hors-contexte.

Bien que les phrases non-projectives ne soient pas la norme, elles existent bel
et bien dans les langues naturelles. Comme le montre la phrase ci-dessus, la
thématisation, qui consiste à placer le thème en début de phrase, peut aboutir à
une structure non-projective. Certaines langues où l'ordre des mots dans la
phrase n'est pas fixé permettent de produire facilement des constructions
non-projectives. La contrainte de projectivité est donc trop forte pour
représenter les structures de dépendances naturelles. Cependant, il n'est pas non
plus pratique de se débarrasser complètement de cette contrainte. Il a été
montré\footcite{neuhaus_complexity_1997} que l'analyse syntaxique avec une
grammaire de dépendances sans restriction sur les phrases projectives est un
problème $NP$-complet.

Heureusement, il semble que les langues naturelles ne produisent pas des phrases
arbitrairement non-projectives. Non seulement celles-ci apparaissent rarement,
mais elles sont limitées par certaines caractéristiques comme la longueur des
discontinuités dans la projection\footcite{yadav_are_2019}. Il est donc
intéressant de chercher des grammaires de dépendances plus expressives grâce à
des restrictions plus faibles sur les phrases
projectives\footcite{gomez-rodriguez_restricted_2016}. C'est dans ce but qu'ont
été définies les grammaires catégorielles de dépendances que je décris dans la
\in{section}[sec:gcd].

\subsection{\em Universal Dependencies}

Lorsqu'on travaille avec des grammaires de dépendances, il est utile de disposer
d'un corpus de phrases annotées. Le projet
{\em Universal Dependencies}\footcite{nivre_universal_2016}\footcite{nivre_universal_2020}
rassemble plus de 200 corpus, dans une centaine de langues différentes dont le
breton\footcite{tyers_prototype_2018}. Ces données sont disponibles sur le site
\href{https://universaldependencies.org/}.

Les corpus d'{\em{}Universal Dependencies} doivent respecter certaines règles
générales afin que les annotations soient le plus universelles possible: deux
phrases dans des langues différentes ayant le même sens doivent recevoir des
structures de dépendances similaires. Pour cela, il faut privilégier les
relations entre les mots qui ont un contenu sémantique, et y subordonner les mots
qui n'ont qu'un rôle syntaxique.

Ces corpus ont permis la création de UDPipe\footcite{straka_udpipe_2016}, une
{\em pipeline} de traitement automatique de langue naturelle qui fait de
l'analyse lexicale, morphologique, de la lemmatisation et de l'analyse syntaxique
en dépendances et en constituants. UDPipe fournit des modèles pour une
cinquantaine de langues du corpus {\em Universal Dependencies} et peut être
entraîné sur les autres grâce à des techniques d'apprentissage statistique.

\section{Grammaires catégorielles} % --------------------------------------------

Les grammaires catégorielles\footcite{moot_logic_2012} sont généralement
considérées comme des grammaires de constituants. Cependant, les grammaires de
Hays et Gaifman, premières grammaires formelles basées sur les dépendances, sont
définies de façon très similaire. Il est donc assez naturel d'utiliser les
grammaires catégorielles pour définir une grammaire de dépendances, comme montré
dans la \in{section}[sec:gcd].

Pour analyser un phrase avec une grammaire catégorielle, on associe une catégorie
à chaque mot de la phrase. Cette catégorie peut être une catégorie primitive
(définie en fonction de la langue) ou une catégorie composée. On peut ensuite
déduire la catégorie d'une suite de mots à partir des catégories des mots qui la
composent. La phrase est acceptée si sa catégorie peut être déduite comme $s$ (la
catégorie de la phrase).

\placefigure[here]{Une phrase dans une grammaire catégorielle}
{
  \setupTABLE[row][each][
    align=middle,
    frame=off,
    height=\lineheight,
  ]
  \setupTABLE[row][1][style=em]
  \setupTABLE[row][2][color=types]
  \bTABLE[loffset=.25em,roffset=.25em]
  \bTR
    \bTD Nous \eTD
    \bTD sommes \eTD
    \bTD tous \eTD
    \bTD des \eTD
    \bTD fleurs \eTD
    \bTD dans \eTD
    \bTD le \eTD
    \bTD même \eTD
    \bTD bouquet.\eTD
  \eTR

  \bTR
    \bTD $np$ \eTD                   % Nous
    \bTD $np \ldep s \rdep np$ \eTD  % sommes
    \bTD $s \ldep s$ \eTD            % tous
    \bTD $np \rdep nn$ \eTD          % des
    \bTD $nn$ \eTD                   % fleurs
    \bTD $s \ldep s \rdep np$ \eTD   % dans
    \bTD $np \rdep n$ \eTD           % le
    \bTD $n \rdep n$ \eTD            % même
    \bTD $n$ \eTD                    % bouquet
  \eTR

  \eTABLE
}

Par exemple, dans la phrase ci-dessus, le mot \quote{fleurs} a pour catégorie
$nn$ (nom pluriel), qui est une catégorie primitive. Le mot \quote{des} a pour
catégorie $np \rdep nn$, qui est une catégorie composée des catégories $np$
(groupe nominal) et $nn$, avec le constructeur $\rdep$. Ce constructeur signifie
que \quote{des} attend à sa droite un mot de type $nn$ pour construire une
expression de type $np$. Le groupe de mot \quote{des fleurs} a donc la catégorie
$np$. Le constructeur $\ldep$ fonctionne symmétriquement à $\rdep$ (avec la
dépendance à gauche).

\placefigure{Une déduction dans le Lambek-calcul} {
  \startformula
  
  \lelim{
    \relim{
      \rintro{
        \lelim{
          \relim{
            \lelim{
              \word{Nous}   {np}
              \word{sommes} {np \ldep s \rdep np}
            }{s \rdep np}

            \hyp{np}
          }{s}

          \word{tous}{s \ldep s}
        }{s}
      }{s \rdep np}

      \relim{
        \word{des}     {np \rdep nn}
        \word{fleurs}  {nn}
      }{np}
    }{s}

    \relim{
      \word{dans}    {s \ldep s \rdep np}

      \relim{
        \word{le}      {np \rdep n}

        \relim{
          \word{même}    {n \rdep n}
          \word{bouquet} {n}
        }{n}
      }{np}
    }{s \ldep s}
  }{s}

  \stopformula
}

On peut remarquer qu'un même mot peut prendre des catégories différentes. Dans
la phrase ci-dessus le mot \quote{dans} introduit un complément circonstantiel et
modifie donc le verbe, c'est pourquoi sa catégorie est $s \ldep s \rdep np$. Dans
une autre phrase, il aurait pu compléter un groupe nominal (par exemple \quote{le
chat {\em dans} la boîte}) et aurait donc la catégorie $np \ldep np \rdep np$.
L'analyse syntaxique doit donc assigner une catégorie à chaque mot de la phrase
parmi l'ensemble des catégories que peut prendre ce mot, de sorte à pouvoir
déduire la catégorie $s$ pour la phrase.

Une grammaire catégorielle définit une syntaxe pour les catégories complexes,
ainsi que des règles de déduction. Un langage est ensuite défini par un ensemble
de types primitifs et un lexique qui associe à chaque mot un ensemble de
catégories que peut avoir ce mot. Les règles sont donc universelles à tous les
langages, qui se distinguent seulement par leur lexique.

\section[sec:gcd]{Grammaires catégorielles de dépendances} % --------------------

Les grammaires catégorielles de dépendances\footcite{dikovsky_dependencies_2004}
(GCD) sont des grammaires catégorielles où les catégories correspondent à des
relations de dépendances. Elles peuvent exprimer des dépendances non-projectives
grâce à des types polarisés. L'algorithme $TGE^{(K)}$ permet d'apprendre de
manière incrémentale certaines sous-classes des
GCD\footcite{bechet_incremental_2021}.

\subsection{Les dépendances comme catégories}

\placefigure[here]
{Une phrase non-projective en GCD}
{
  \switchtobodyfont[10pt]

  \startnodes[dx=1em,dy=2.75ex,alternative=arrow]
    
    \cat { 0}   {Une}        {Une}        {$det$}
    \cat { 5}   {citrouille} {citrouille} {$\rppol{det \ldep suj}{acl}$}
    \cat { 9.5} {est}        {est}        {$aux$}
    \cat {15}   {apparue}    {apparue}    {$suj \ldep aux \ldep S \rdep \ranchor{acl}$}
    \cat {20}   {qui}        {qui}        {$suj$}
    \cat {23}   {n}          {n'}         {$adv$}
    \cat {27.5} {etait}      {était}      {$adv \ldep cop \rdep adv$}
    \cat {32}   {pas}        {pas}        {$adv$}
    \cat {39}   {la}         {là}         {$\rnpol{suj \ldep cop \ldep \ranchor{acl} \rdep adv}{acl}$}
    \cat {46}   {avant}      {avant.}     {$adv$}

    \placenode [15,4] [reference=root] {\node{\tt root}}

    \rootis{apparue}

    \depend{citrouille, Une}        {det} {-0.35}
    \depend{citrouille, la}         {acl} { 0.35}
    \depend{apparue,    citrouille} {suj} {-0.2}
    \depend{apparue,    est}        {aux} { 0.2}
    \depend{etait,      n}          {adv} { 0.35}
    \depend{etait,      pas}        {adv} {-0.35}
    \depend{la,         qui}        {suj} {-0.2}
    \depend{la,         etait}      {cop} {-0.2}
    \depend{la,         avant}      {adv} { 0.2}
    
    \connectnodes [apparue, la][label={$\anc{\prn{acl}}$}, offset=0.25, option=dashed]
  \stopnodes
}

Les catégories primitives de la grammaire catégorielle de dépendances
contiennent les noms des dépendances issues de l'analyse en grammaire de
dépendances. En présence de dépendances projectives, on construit la catégorie du
mot $w$ de la façon suivante:
\startitemize
\item soient $l_1,…,l_k$ les noms des dépendances dans lesquelles $w$ est
      gouverneur et qui pointent à gauche de $w$;
\item soient $r_1,…,r_m$ les noms des dépendances dans lesquelles $w$ est
      gouverneur et qui pointent à droite de $w$;
\item soit $h$ le nom de la dépendance à laquelle $w$ est subordonné, ou $S$ si
      $w$ est la racine;
\item alors le type de $w$ est:
      $l_1 \ldep … \ldep l_k \ldep h \rdep r_1 \rdep … \rdep r_m$.
\stopitemize
Par exemple le mot \quote{était} dans la phrase ci-dessus gouverne un adverbe à
gauche et un adverbe à droite, et est subordonné en tant que copule. Sa catégorie
est donc $adv \ldep cop \rdep adv$.

Cette manière de catégoriser ne fonctionne que lorsque les dépendances sont
projectives. Pour intégrer les dépendances non-projectives, on utilise les
valences polarisés. Si $d$ est le nom d'une dépendance non-projective, alors les
expressions $\pln{d}$, $\plp{d}$, $\prn{d}$ et $\prp{d}$ sont des valences
polarisées. L'orientation gauche-droite de la flèche représente la direction de
la dépendance non-projective par rapport au mot. La flèche pointe vers le haut
s'il s'agit d'une dépendance où le mot est gouverneur (valence positive) et vers
le bas si le mot est subordonné par cette dépendance (valence négative).

La catégorie d'un mot est de la forme $B^P$, où $B$ est le type de base qui
correspond aux dépendances projectives, et $P$ est le potentiel, c'est-à-dire une
suite de valences polarisées qui correspondent aux dépendances non-projectives.
Par exemple le mot \quote{citrouille} gouverne un déterminant à gauche et est
subordonné comme sujet, son type de base est donc $det \ldep suj$. De plus, il
gouverne une proposition subordonnée par une dépendance non-projective, donc son
potentiel est $\prp{acl}$. Le type complet est $\rppol{det \ldep suj}{acl}$.

Lorsqu'on construit le type d'un groupe de mots, les potentiels sont concaténés.
Pour éliminer une valence du potentiel, il faut l'associer à sa valence
{\em duale}; il s'agit de la valence ayant le même nom de dépendance et la même
direction, mais de signe opposé. Cette association se fait selon la règle de la
{\em première disponible}: on élimine les valences dès qu'une association est
possible.

Lorsqu'un mot est subordonné par une dépendance non-projective $d$, il produit
une {\em ancre}. Celle-ci est notée $\anc{\pln{d}}$ ou $\anc{\prn{d}}$ selon la
direction de $d$. Elle apparait dans le type de base du mot, et sera neutralisé
par une dépendance projective avec le mot de la phrase qui accepte une ancre de
ce type (en pointillés sur le schéma). Par exemple le mot \quote{là} est
subordonné à \quote{citrouille} par une dépendance non-projective, il produit
donc l'ancre $\anc{\prn{acl}}$ une fois ses dépendances projectives satisfaites.
C'est le mot \quote{apparue} qui accepte cette ancre à sa droite.

Il y a un dernier cas à considérer: celui des séquences
itérées\footcite{amblard_categorial_2016}. Certaines dépendances peuvent
apparaître un nombre illimité de fois de suite, comme les compléments
circonstanciels ou les énumérations. Dans ce cas, il n'est pas pratique d'ajouter
au lexique l'infinité de combinaisons possibles selon le nombre de fois où cette
dépendance apparait. On peut résumer cela dans un type itéré: $d*$ correspond à
une dépendance qui peut apparaître un nombre indéterminé de fois (y compris
zéro).

\placefigure[here]
{Une phrase avec une dépendance itérée}
{
  \startnodes[dx=1em,dy=2.75ex,alternative=arrow]

    \lemma{ 0}    {Je}         {Je}
    \lemma{ 1.25} {te}         {te}
    \lemma{ 3.75} {donnerai}   {donnerai}
    \lemma{ 6.5}  {un}         {un}
    \cat  {10}    {frigidaire} {frigidaire,} {$det \ldep obj \rdep conj*$}
    \lemma{13.5}  {un2}        {un}
    \lemma{15.75} {joli}       {joli}
    \lemma{19}    {scooter}    {scooter,}
    \lemma{22}    {un3}        {un}
    \lemma{25}    {atomixer}   {atomixer,}
    \lemma{28}    {et}         {et}
    \lemma{29.5}  {du}         {du}
    \lemma{33}    {Dunlopillo} {Dunlopillo.}

    \depend{donnerai,frigidaire}   {obj}  {0.4}
    \depend{frigidaire,un}         {det}  {0.4}
    \depend{frigidaire,scooter}    {conj} {0.2}
    \depend{frigidaire,atomixer}   {conj} {0.2}
    \depend{frigidaire,Dunlopillo} {conj} {0.2}
  \stopnodes
}

\subsection{Apprentissage}

Les grammaires catégorielles de dépendances avec des types itérés ne sont pas en
général apprenables à partir d'exemples positifs. Cependant, certaines
sous-classes le sont. C'est le cas des GCD $K$-étoile révélatrices. Ces GCD ne
font pas la différence entre une dépendance qui peut apparaître jusqu'à $n$ fois
qand $n > K$ et un dépendance itérée. En pratique, cela correspond assez bien à
ce qui se passe dans les langues naturelles.

Cependant un deuxième problème se pose. En effet, le critère de définition des
GCD $K$-étoile révélatrices n'est pas facile à déterminer algorithmiquement (s'il
n'est pas indécidable). Un critère plus simple permet de
définir les grammaires $K$-étoile simples\footcite{bechet_simple_2016} grâce à
une vérification syntaxique sur les catégories du lexique.

Tout cela a permis de construire l'algorithme $TGE^{(K)}$. Pour chaque nouvel
exemple, cet algorithme effectue les opérations suivantes:
\startitemize[n]
\item il calcule les types de chaque mot à partir de la structure de dépendances;
\item il généralise les types obtenus pour satisfaire le critère $K$-étoile
      simple;
\item il ajoute les types obtenus au lexique de la grammaire.
\stopitemize

\section{Travail à réaliser} % --------------------------------------------------

Le premier objectif est d'implémenter l'algorithme $TGE^{(K)}$. Il faudra ensuite
évaluer les grammaires obtenues en les comparant par exemple aux modèles UDPipe.
Il serait ausssi intéressant d'étudier l'expressivité des grammaires $K$-étoile
simple par rapport aux autres grammaires de dépendances.