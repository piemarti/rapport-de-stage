\chapter{Vers un outil pour l'aide à l'apprentissage du breton} % ===============

La langue bretonne est une langue peu dotée en ressources linguistiques; elle est
considérée par l'UNESCO comme
\quote{sévèrement en danger}\footcite{moseley_atlas_2010}. Un des objectifs de ce
stage est de faciliter l'apprentissage de cette langue en développant un outil
d'aide à la lecture. Je commencerai cette partie par une description de la
situation du breton, puis je présenterai le concept proposé de
\quote{lecture augmentée}. Dans la troisième section, je justifie l'intérêt de ce
concept pour l'apprentissage de seconde langue grâce à la théorie de
l'{\em input processing}. Finalement je conclus par une description du travail à
réaliser.

\section{Contexte: la situation du breton} % ------------------------------------

Dans \cite[title][broudic_langue_2013], \cite[authoryears][broudic_langue_2013]
décrit l'évolution de la situation du breton depuis le début du XXè siècle. De
1902 à 1952, la part de la population de Basse-Bretagne qui parle exclusivement
breton passe de 50\% à 7\%. Si dans les années 50, la population est aux
trois-quart bilingue, le nombre de locuteurs du breton s'effondre dans la
deuxième moitié du XXè siècle, jusqu'à ce qu'en 2007, seuls 13\% de la population
connaît le breton, parmi lesquels 70\% ont plus de 60 ans.

Cet effondrement est lié à une dévaluation massive de la langue. Avec
l'instauration de l'enseignement primaire obligatoire en français, le
développement du tourisme, la Bretagne est de plus en plus en contact avec la
langue française. Dans les années 50, les commerçants et les paysans aisés
commencent à élever leurs enfants en français, qui devient une langue
prestigieuse associée à la modernité et au progrès. Cela a conduit à
l'interruption de la transmission intergénérationnelle du breton.

À cela s'ajoute le rapport particulier de la France à la langue française: ici
plus qu'ailleurs la langue officielle est conçue comme intrinsèquement liée à
l'identité nationale et aux valeurs associées à la
république\footcite{joubert_aurelie_introduction_2019}. Le principe
d'indivisibilité de la république est par exemple invoqué en 1999 par le Conseil
Constitutionnel pour censurer la ratification de la charte Européenne pour les
langues régionales ou minoritaires.

Cependant, depuis les années 70, les langues non-officielles bénéficient de plus
en plus de reconnaissance. En 2001, la délégation générale à la langue
française du ministère de la culture devient la délégation générale à la langue
française {\em et aux langues de France}, et la réforme constitutionnelle de 2008
reconnaît les langues régionales comme faisant partie du patrimoine culturel
national. Les activistes des langues régionales, qui s'opposaient autrefois à
l'action de l'État, sont aujourd'hui les acteurs de politiques linguistiques
institutionnelles\footcite{o_hifearnain_institutional_2013}.

En 2007, un cinquième seulement des bretonnants pratiquent le breton dans la vie
courante. La langue bretonne \quote{n’est plus réellement perçue comme un moyen
de communication et d’échange, comme si elle était d’ores et déjà exclue du champ
de la nécessité} \cite[authoryear][broudic_langue_2013]. Les arguments en faveur
de sa revitalisation en appellent à la préservation d'un patrimoine culturel, à
l'affirmation d'une identité régionale ou à son attrait esthétique plutôt qu'à son
utilité comme moyen de communication. Cette situation donne lieu à un usage
post-vernaculaire\footcite{shandler_postvernacular_2004}\footcite{hornsby_finding_2017}
du breton: la langue n'est plus une langue de tous les jours mais devient un
moyen d'exprimer une distinction culturelle qu'on utilise dans des espaces de
performance dédiés comme les festoù-noz.

\section{La lecture augmentée} % ------------------------------------------------

L'outil proposé pour ce stage est une application de lecture augmentée. Ce
concept est inspiré des interfaces utilisateur de désambiguïsation intéractive.
Par exemple dans Google Traduction, des bulles apparaissent au survol d'un mot,
ce qui permet à l'utilisateur d'intéragir avec le texte en séléctionnant la
traduction qui lui convient le mieux, ou en en proposant une nouvelle.

Dans un système de lecture augmentée, ce genre d'intéraction est en lien avec la
lecture plutôt que la traduction. Plusieurs types d'informations peuvent être
superposées au texte: traduction de mots, liens Wikipedia, structures de
dépendances des phrases, coréférences, liens sémantiques… L'utilisateur peut
naviguer entre plusieurs vues sur le texte et y introduire des modifications ou
ajouter des notes.

La lecture augmentée est l'étape suivante des systèmes de lecture active comme
LiquidText\footcite{tashman_liquidtext_2011}. Ceux-ci permettent à l'utilisateur
d'intéragir avec le texte de façon active grâce à des techniques de navigation
avancées, la possibilité d'annoter le texte et d'en extraire des passages. À
cela, la lecture augmentée ajoute le calcul automatique d'informations concernant
le texte.

\section{Justification théorique: la théorie {\em input processing}} % ----------

Les {\em inputs}, c'est-à-dire l'ensemble des textes et paroles lus ou écoutées
par l'apprenant, jouent un rôle central dans l'acquisition d'une seconde langue.
En améliorant l'intéraction de l'apprenant avec ces {\em inputs}, il est possible
que la lecture augmentée ait un effet positif sur l'apprentissage de la langue.
La théorie {\em input processing} de VanPatten\footcite{vanpatten_input_2007}
fournit un cadre théorique pour comprendre comment produire cet effet.

La théorie {\em input processing} part du principe que l'acquisition d'une langue
seconde chez l'adulte passe au moins en partie par la création d'associations
entre signifiants et signifiés lors de la compréhension. Son objet est de décrire
ce qui se passe lors de la compréhension d'un {\em inputs}. Des études empiriques
ont permis d'établir les principes suivants:

\startitemize[n]
\item Les apprenants traitent en priorité les mots avec un contenu sémantique en
      premier.
\item Les apprenants traitent les unités lexicales avant les formes grammaticales
      (terminaisons, accords,…).
\item Les apprenants traitent en priorité les formes grammaticales dont le
      contenu sémantique n'est pas redondant avec un mot.
\item Les apprenants traitent en priorité les formes grammaticales qui ont un
      contenu sémantique (contrairement par exemple à l'accord en genre en
      français).
\item Les apprenants ont tendance à traiter le premier nom rencontré comme le
      sujet (il n'est pas clair si c'est universellement le cas ou si c'est une
      influence de la langue maternelle)
\item Le sujet de la phrase est aussi déterminé en fonction de la compatibilité
      lexicale du nom (est-ce que ce nom peut être le sujet de l'action), de la
      probabilité de l'évennement décrit, ou du contexte.
\item Les apprenants traitent en priorité le début de la phrase, puis la fin,
      puis le milieu.
\stopitemize

Ces principes permettent de déterminer quels types d'erreurs les apprenants sont
plus susceptibles de faire et quels types de phrases posent le plus de
difficulté. Par exemple, selon le principe 5, ils auront plus de mal à comprendre
les constructions passives. Pour contrebalancer ces erreurs, VanPatten propose la
technique des
{\em processing instructions}\footcite{liontas_processing_2018}:
il s'agit de guider l'apprenant dans la compréhension de l'{\em input} en
altérant celui-ci de sorte à modifier la stratégie de traitement de
l'utilisateur, et en fournissant des indications explicites. Cette technique
pourrait être implémentée dans un système de lecture augmentée.

\section{Travail à faire} % -----------------------------------------------------

Le stage devra donner lieu à la réalisation d'un prototype pour un système de
lecture augmentée. On pourra utiliser des grammaires catégorielles de dépendances
pour analyser la syntaxe des phrases et utiliser la théorie
{\em input processing} pour déterminer quelles augmentations apporter. Bien que la
durée du stage ne permette pas de mesurer les effets de long terme sur
l'acquisition du breton, il serait intéressant d'effectuer une expérimentation
du dispositif.