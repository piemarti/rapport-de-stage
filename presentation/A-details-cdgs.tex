\chapter{Détails sur les grammaires catégorielles de dépendances} % =============

\section{Définition formelle} % -------------------------------------------------

Soit $C$ l'ensemble des noms des dépendances projectives (avec $s \in C$) et $V$
l'ensemble des noms des dépendances non-projectives.

\startdefinition{Valences polarisées}
Pour chaque $v \in V$, on définit les valences polarisées
$\pln{v}, \plp{v}, \prn{v}, \prp{v}$. Les valences $\plp{v}$ et $\prp{v}$ son
dites positives, et les valences $\pln{v}$ et $\prn{v}$ sont négatives. Deux
valences polarisées avec le même nom et qui pointent du même côté, mais dont
l'une est positive et l'autre négative sont duales.
\stopdefinition

\startdefinition{Ancres}
Pour chaque valence polarisée négative $v$, on définit un type ancre $\#v$.
\stopdefinition

\startdefinition{Dépendances itérées}
Pour chaque $d \in C$, on définit un type de dépendance itérée $d^*$.
\stopdefinition

\startdefinition{Catégories de base de la GCD}
Les types primitifs de la GCD sont les types de $C$, les types ancre et les types
de dépendance itérée. Si
\startitemize
\item $l_1,…,l_m$ et $r_1,…,r_m$ sont des suites de types primitifs (possiblement
      vides),
\item et $h$ est un type primitif qui n'est pas itéré,
\stopitemize
alors $l_m \ldep … \ldep l_1 \ldep h \rdep r_1 \rdep … \rdep r_m$ est un type de
base de la grammaire catégorielle de dépendances.
\stopdefinition

\startdefinition{Catégories de la GCD}
Un potentiel est une suite (possiblement vide) $v_1,…,v_p$ de valences
polarisées. Si $l_m \ldep … \ldep l_1 \ldep h \rdep r_1 \rdep … \rdep r_m$ est un
type de base de la GCD, alors
\startformula
\left[ l_m \ldep … \ldep l_1 \ldep h \rdep r_1 \rdep … \rdep r_m \right]^{v_1,…,v_p}
\stopformula
est une catégorie de la GCD. On not $CAT(C,V)$ l'ensemble des types de
dépendances.
\stopdefinition

\startdefinition{Grammaire catégorielle de dépendance}
Une grammaire catégorielle de dépendance $G = (W,C,V,λ)$ est définie par:
\startitemize
\item un ensemble fini $W$ de mots,
\item un ensemble fini $C$ de noms de dépendances projectives qui contient $s$,
\item un ensemble fini $V$ de noms de dépendances non-projectives,
\item et un lexique $λ : W \to 𝒫(CAT(C,V))$ qui associe à chaque mot de $W$ un
      ensemble de types de dépendances.
\stopitemize
\stopdefinition

\section{Règles de déductions} % ------------------------------------------------

Les règles du calcul des types de dépendances sont relatives à la position du mot
dans la phrase. On note $i : B^P$ un mot à la position $i$ dont le type est
$B^P$. Le symbole $c$ dénote une dépendance projective de $C$, les symboles $P$,
$Q$ et $R$ dénotent des potentiels et le symbole $β$ dénote un type de base
quelconque.


\startformulas
  \startformula
    \frac{ i : c^P, \, j : \left[ c \ldep β \right]^Q }
         { j : β^{P,Q} }
    \mathbf{L^l}
  \stopformula

  \startformula
    \frac{ j : \left[ β \rdep c \right]^P, \, i : c^Q }
         { j : β^{P,Q} }
    \mathbf{L^r}
  \stopformula
\stopformulas

\startformulas
  \startformula
    \frac{ i : c^P, \, j : \left[ c^* \ldep β \right]^Q }
         { j : \left[ c^* \ldep β \right]^{P,Q} }
    \mathbf{I^l}
  \stopformula

  \startformula
    \frac{ j : \left[ β \rdep c^* \right]^P, \, i : c^P }
         { j : \left[ β \rdep c^* \right]^{P,Q} }
    \mathbf{I^r}
  \stopformula
\stopformulas

\startformulas
  \startformula
    \frac{ i : \left[ c^* \ldep β \right]^P }
         { i : β^P }
    \mathbf{Ω^l}
  \stopformula

  \startformula
    \frac{ i : \left[ β \rdep c^* \right]^P }
         { i : β^P }
    \mathbf{Ω^r}
  \stopformula
\stopformulas

\startformulas
  \startformula
    \frac{ i : β^{P,(l:↙v),Q,(k:↖v),R} }
         { i : β^{P,Q,R} }
    \mathbf{D^l}
  \stopformula

  \startformula
    \frac{ i : β^{P,(k:↗v),Q,(l:↘v),R} }
         { i : β^{P,Q,R} }
    \mathbf{D^r}
  \stopformula
\stopformulas

Les règles $\mathbf{D^l}$ et $\mathbf{D^r}$ s'appliquent si $k < l$, et si $Q$ ne
contient pas de valence polarisée qui permettrait d'appliquer la règle sur une
distance plus courte.

\startdefinition{Structure de dépendances induite par une preuve}
Soit $ρ$ une preuve de la phrase $x$ dans le calcul des grammaire catégorielle de
dépendances représentée comme une suite d'applications de règles. La structure de
dépendances induite par $ρ$ est représenée par un graphe orienté
$DS_x = (W,E)$ défini ainsi:

\startitemize
\item $DS_x(ε) = (W, ∅)$
\item $DS_x(ρ,R) = (W, DS_x(ρ) ∪ d)$ où $d$ est:
  \startitemize
  \item $\{j \xrightarrow{c} i\}$ si $R$ est une occurrence de $\mathbf{L}$ ou
        de $\mathbf{I}$
  \item $\{k \xrightarrow{c} l\}$ si $R$ est une occurrence de $\mathbf{D}$
  \item $∅$ si $R$ est une occurrence de $\mathbf{Ω}$
  \stopitemize
\stopitemize
\stopdefinition

\startdefinition{Langage défini par une GCD}
Soit $G = (W,C,V,λ)$ une grammaire catégorielle de dépendances, $D$ une
structure de dépendances et $x$ une phrase.

On note $G\left[D,x\right]$ s'il existe une preuve $ρ$ de $x$ dans $G$ telle que
$D = DS_x(ρ)$.

Le langage défini par $G$ est l'ensemble
$L(G) = \{x ∈ W^* | ∃D, G\left[D,x\right]\}$ des phrases auxquelles $G$ associe
une structure de dépendances.

Le langage de dépendances défini par $G$ est l'ensemble
$Δ(G) = \{D | ∃ x, G\left[D,x\right]\}$ des structures de dépendances que $G$
peut associer à une phrase.

On note $𝓛(CDG)$ la classe des langages définis par une GCD et $𝒟(CDG)$ la classe
des langages de dépendances définis par une GCD.
\stopdefinition

\startdefinition{Équivalence des GCD}
Deux grammaires catégorielles de dépendances $G_1$ et $G_2$ sont équivalentes au
sens faible si $𝓛(G_1) = 𝓛(G_2)$.

Deux grammaires catégorielles de dépendances $G_1$ et $G_2$ sont équivalentes au
sens fort si $Δ(G_1) = Δ(G_2)$.
\stopdefinition

\section{Apprenabilité et algorithme $TGE^{(K)}$} % ------------------------------
\placefigure[here]{L'algorithme $TGE^{(K)}$}
{
  \startlines
  {\bf Entrée: } une séquence σ de structures de dépendances
  {\bf Sortie: } une grammaire $K$-étoile révélatrice $G$
  \startlinenumbering[algorithm]
  G \gets (W, C, V, λ)
  \quad avec W = ∅, C = \{s\}, V = ∅ et λ : w \mapsto ∅
  {\bf Pour chaque} $D = (W,E) \in σ$
  \quad {\bf Pour chaque} $w \in W$
  \quad \quad $W \gets W ∪ \{ w \}$
  \quad \quad $t_w \gets V(w, D)$ la proximité de $w$ dans $D$
  
  \quad \quad {\bf Tant que} $t_w = \left[ α \ldep d \ldep … \ldep d \ldep β \right]^P$
  \quad \quad \quad $t_w \gets \left[ α \ldep d^* \ldep β \right]^P$
  \quad \quad {\bf Fin Tant que}
  
  \quad \quad {\bf Tant que} $t_w = \left[ α \rdep d \rdep … \rdep d \rdep β \right]^P$
  \quad \quad \quad $t_w \gets \left[ α \rdep d^* \rdep β \right]^P$
  \quad \quad {\bf Fin Tant que}

  \quad \quad $λ(w) \gets λ(w) ∪ \{t_w\}$
  \quad {\bf Fin Pour}
  {\bf Fin Pour}
  \stoplinenumbering
  \stoplines
}

L'apprenabilité d'une grammaire est définie de la façon suivante:

\startdefinition{Apprentissage à la limite}
Soit $Φ(G)$ un ensemble d'exemples de $G$ (des phrases de $L(G)$ ou des
structures de dépendances de $D(G)$) et $σ$ une séquence de $Φ(G)$.

Soit $A$ un algorithme qui pour chaque sous-séquence $σ\left[1…i\right]$ d'une
séquence $σ$ de $Φ(G)$ calcule une GCD $A(σ\left[1…i\right])$.

$A$ apprend une grammaire $G$ si pour toute séquence $σ$, il existe un entier
$T>0$ tel que $\forall t>T, A(σ\left[1…t\right])$ est fortement équivalent à $G$.

Une grammaire $G$ est apprenable s'il existe un algorithme qui l'apprend à la
limite.
\stopdefinition

\startdefinition{Généralisation $K$-étoile}
Soit un entier $K>1$. On définit une CDG $𝒞^K(G)$, la généralisation $K$-étoile
de $G$ de la manière suivante: pour chaque mot $w$ ayant une catégorie
$t \in λ(w)$ de la forme $L \ldep t_1 \ldep … \ldep t_k \ldep M \ldep h \rdep R$
on ajoute à $λ(w)$ les catégories $L \ldep M \ldep h \rdep R$ et
$L \ldep d^* \ldep M \ldep h \rdep R$. On fait de même lorsque la séquence $t_1,…,t_k$
apparaît à droite.
\stopdefinition

\startdefinition{Grammaire $K$-étoile révélatrice}
Une CDG $G$ est $K$-étoile révélatrice si $G=𝒞(G)$.

On note $CDG^{K→*}$ la classe des grammaires $K$-étoile révélatrices.
\stopdefinition

On calcule le type correspondant à un mot de la façon suivante:

\startdefinition{Proximité}
Soit $D = (W, E)$ une structure de dépendances et $w \in W$ un mot qui a:
\startitemize
\item une dépendance nommée $h$ qui arrive sur $w$ (ou la racine $s$),
\item une séquence de dépendances projectives à gauche $l_1,…,l_k$,
\item une séquence de dépendances projectives à droite $r_1,…,r_m$,
\item une séquence de dépendances non-projectives $d_1,…,d_n$ avec les
      polarités respectives $p_1,…,p_n$.
\stopitemize

La proximité de $w$ dans $D$ est le type:
\startformula
\left[ l_1 \ldep … \ldep l_k \ldep h \rdep r_1 \rdep … \rdep r_m \right]^P
\stopformula
où $P$ est une permutation de $p_1d_1,…,p_nd_n$ dans un ordre lexicographique
compatible avec l'ordre des polarités: $↖ < ↘ < ↙ < ↗$.
\stopdefinition

Les grammaires $K$-étoile révélatrices sont apprenables à la limite à partir de
structures de dépendances. Comme on ne peut pas en pratique s'assurer que les
exemples donnés en entrée correspondent bien à une grammaire $K$-étoile
révélatrice, on définit une sous-classe appelée $K$-étoile simple:

\startdefinition{Grammaire $K$-étoile simple}
Soit un entier $K>1$, soit $t$ une catégorie et $d$ le nom d'une dépendance. $t$
est $K$-étoile simple à gauche sur $d$ si pour toute occurrence
$l_1 \ldep … \ldep l_k$ avec $l_i=d$ ou $l_i=x^*$, il y a au plus $K-1$
occurrences de $d$, et aucune occurrence de $d$ s'il y a une occurrence de $d^*$.

On définit $K$-étoile simple à droite de façon similaire. $t$ est $K$-étoile
simple s'il est $K$-étoile simple à droite et à gauche.

Une grammaire $G$ est $K$-étoile simple si toutes ses catégories sont $K$-étoile
simple.

On note $CDG^{K\lettertilde *}$ la classe des langages $K$-étoile simples.
\stopdefinition

L'algorithme $TGE^{(K)}$ apprend à la limite les grammaire $K$-étoile simples de
façon incrémentale, dans le sens suivant:

\startdefinition{Apprentissage incrémental}
L'ensemble $Δ(G)$ est monotone par rapport à un ordre partiel $≼$ si pour toute
grammaires $G_1$ et $G_2$, $G_1 ≼ G_2 \implies Δ(G_1) ⊆ Δ(G_2)$. Soit $≼$ un
tel ordre.

Soit $A$ un algorithme d'inférence à partir de structures de dépendances et $σ$
une séquence d'exemples pour une grammaire $G$.

\startitemize
\item $A$ est monotone si $A(σ\left[1…i\right]) ≼ A(σ\left[1…j\right])$ pour tout
      $i$ et $j$..
\item $A$ est fidèle si $Δ(A(σ\left[1…i\right])) ⊆ Δ(G)$ pour tout $i$.
\item $A$ est expansif si $σ\left[1…i\right] ⊆ Δ(A(σ\left[1…i\right]))$ pour tout $i$.
\stopitemize

Un algorithme d'apprentissage est incrémental s'il est monotone, fidèle et
expansif.
\stopdefinition